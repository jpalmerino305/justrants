import * as postActionTypes from '../constants/PostActionTypes';

let initialState = {
  loading: []
};

const posts = (state = initialState, action) => {
  switch(action.type) {

    case postActionTypes.LOADING:
      return { ...state, loading: action.payload };

    default:
      return state;

  }
};

export default posts;
