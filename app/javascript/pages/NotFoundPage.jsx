import React from 'react';
import PropTypes from 'prop-types';

class NotFoundPage extends React.Component {

  render () {
    return (
      <React.Fragment>
        <h1>404 Page Not Found</h1>
      </React.Fragment>
    );
  }
}

export default NotFoundPage;
