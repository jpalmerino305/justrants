import * as postActionTypes from '../constants/PostActionTypes';

export const loading = (isLoading) => (dispatch) => {
  dispatch({ type: postActionTypes.LOADING, payload: isLoading });
};
