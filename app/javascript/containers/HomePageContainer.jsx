import { connect } from 'react-redux';
import * as postActions from '../actions/PostActions';
import HomePage from '../pages/HomePage';


const mapStateToProps = (state) => ({
  loading: state.post.loading,
});

const mapDispatchToProps = (dispatch) => ({
  loading: (isLoading) => dispatch(postActions.loading(isLoading))
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
