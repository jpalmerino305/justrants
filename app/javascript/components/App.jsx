import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { applyMiddleware, compose, createStore, combineReducers } from 'redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import thunk from 'redux-thunk';
import postReducer from '../reducers/PostReducer';
import HomePageContainer from '../containers/HomePageContainer';
import NotFoundPage from '../pages/NotFoundPage';

const middlewares = [];
const rootReducer = combineReducers({
  post: postReducer
});
const store = compose(applyMiddleware(...middlewares))(createStore)(rootReducer);

class App extends React.Component {
  render () {
    return (
      <React.Fragment>
        <Provider store={store}>
          <Router>
            <Link to="/">Home</Link>

            <Switch>
              <Route path="/" exact component={HomePageContainer} />
              <Route component={NotFoundPage} />
            </Switch>
          </Router>
        </Provider>
      </React.Fragment>
    );
  }
}

export default App;
